<?php

class Lesson extends \Eloquent {

    protected $fillable = ['title', 'body', 'active'];

    public function tags()
    {
        return $this->belongsToMany('Tag');
    }
}