<?php

use Faker\Factory as Faker;

class LessonsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 20) as $index)
        {
            Lesson::create([
                'title' => $faker->sentence(5),
                'body' => $faker->paragraph(4),
                'active' => $faker->boolean()
            ]);
        }
    }

}
