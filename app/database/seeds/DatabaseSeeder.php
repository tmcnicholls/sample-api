<?php

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder {

    /**
     * @var array
     */
    private $tables = [
        'lessons',
        'lesson_tag',
        'tags',
        'users'
    ];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $this->cleanDatabase();

        Eloquent::unguard();

        $this->call('LessonsTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('TagsTableSeeder');
        $this->call('LessonTagTableSeeder');
	}

    /**
     *
     */
    private function cleanDatabase()
    {
        // Set foreign key checks to 0 so we can truncate the lesson_tag table before seeding data
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach($this->tables as $tableName)
        {
            DB::table($tableName)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
