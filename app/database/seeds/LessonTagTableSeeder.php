<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LessonTagTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        $lessonIDs = Lesson::lists('id');
        $tagIDs = Tag::lists('id');

        foreach (range(1, 30) as $index) {
            DB::table('lesson_tag')->insert([
                'lesson_id' => $faker->randomElement($lessonIDs),
                'tag_id'    => $faker->randomElement($tagIDs)
            ]);
        }
    }

}