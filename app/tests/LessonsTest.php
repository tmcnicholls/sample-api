<?php

/**
 * Class LessonsTest
 */
class LessonsTest extends APITester {

    public function test_it_fetches_lessons()
    {
        // set up lesson
        $this->times(5)->make('Lesson');
        // call API endpoint
        $this->getJSON('api/v1/lessons');
        // assert we got a 200 response
        $this->assertResponseOk();
    }

    public function test_it_fetches_a_single_lesson()
    {
        // set up lesson
        $this->make('Lesson');
        // call API endpoint
        $lesson = $this->getJSON('api/v1/lessons/1')->data;
        // assert we got a 200 response
        $this->assertResponseOk();
        // assert the object has the correct fields
        $this->assertObjectHasAttributes($lesson, 'title', 'body', 'active');
    }

    public function test_it_404s_if_a_lesson_is_not_found()
    {
        // call API endpoint of a lesson that doesn't exist
        $json = $this->getJSON('api/v1/lessons/10');
        // assert we got a 404 response
        $this->assertResponseStatus(404);
        // assert we get an error field as part of the response
        $this->assertObjectHasAttributes($json, 'error');
    }

    public function test_it_creates_a_lesson_given_valid_input()
    {
        // Post valid creation data from our object stub
        $this->getJson('api/v1/lessons', 'POST', $this->getStub());
        // Assert we get a 201 created response
        $this->assertResponseStatus(201);
    }

    public function test_it_throws_400_error_if_validation_fails()
    {
        // Post invalid creation data to our lessons creation endpoint
        $this->getJson('api/v1/lessons', 'POST');
        // Assert we get a 400 invalid request response
        $this->assertResponseStatus(400);
    }

    /**
     * @return array
     */
    protected function getStub()
    {
        return [
            'title' => $this->fake->sentence,
            'body' => $this->fake->paragraph,
            'active' => $this->fake->boolean
        ];
    }

}
