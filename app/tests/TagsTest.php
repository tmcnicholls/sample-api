<?php

/**
 * Class TagsTest
 */
class TagsTest extends APITester {

    public function test_it_fetches_tags()
    {
        // set up tags
        $this->times(5)->make('Tag');
        // call API endpoint
        $this->getJSON('api/v1/tags');
        // assert we got a 200 response
        $this->assertResponseOk();
    }

    public function test_it_creates_a_tag_given_valid_input()
    {
        // Post valid creation data from our object stub
        $this->getJson('api/v1/tags', 'POST', $this->getStub());
        // Assert we get a 201 created response
        $this->assertResponseStatus(201);
    }

    public function test_it_throws_400_error_if_validation_fails()
    {
        // Post invalid creation data to our lessons creation endpoint
        $this->getJson('api/v1/tags', 'POST');
        // Assert we get a 400 invalid request response
        $this->assertResponseStatus(400);
    }

    /**
     * @return array
     */
    protected function getStub()
    {
        return [
            'name' => 'testtag'
        ];
    }

}
