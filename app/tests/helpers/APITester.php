<?php

use Faker\Factory as Faker;

/**
 * Class APITester
 */
abstract class APITester extends TestCase
{
    use Factory;

    /**
     * @var \Faker\Generator
     */
    protected $fake;

    function __construct()
    {
        $this->fake = Faker::create();
    }

    /**
     * Set up testing environment and migrate in memory database
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->app['artisan']->call('migrate');
    }


    /**
     * Get JSON response from API
     *
     * @param $uri
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    protected function getJson($uri, $method = 'GET', $parameters = [])
    {
        return json_decode($this->call($method, $uri, $parameters)->getContent());
    }

    /**
     * Assert the object has the stated attributes
     *
     */
    protected function assertObjectHasAttributes()
    {
        $args = func_get_args();
        $object = array_shift($args);

        foreach ($args as $attribute) {
            $this->assertObjectHasAttribute($attribute, $object);
        }
    }

}