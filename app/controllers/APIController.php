<?php

use \Illuminate\Http\Response as IlluminateResponse;
use \Illuminate\Pagination\Paginator as Paginator;

/**
 * Class APIController
 */
class APIController extends \BaseController {

    /**
     * @var int
     */
    protected $statusCode = IlluminateResponse::HTTP_OK;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found.')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function respondInternalError($message = 'Internal Error.')
    {
        return $this->setStatusCode(IlluminateResponse::INTERNAL_SERVER_ERROR)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function respondFailedValidation($message = "Validation Failed.")
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function respondCreated($message = "Created Successfully.")
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_CREATED)->respond([
            'message' => $message
        ]);
    }

    /**
     * Returns an API response
     *
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Returns an API response with the pagination object
     *
     * @param Paginator $resource
     * @param $data
     * @return mixed
     */
    protected function respondWithPagination(Paginator $resource, $data)
    {
        $data = array_merge($data, [
            'paginator' => [
                'total_count' => $resource->getTotal(),
                'total_pages' => ceil($resource->getTotal() / $resource->getPerPage()),
                'current_page' => $resource->getCurrentPage(),
                'limit' => $resource->getPerPage()
            ]
        ]);

        return $this->respond($data);
    }

    /**
     * Returns an API error response
     *
     * @param $message
     * @return mixed
     */
    public function respondwithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

}